import * as Math from "mathjs";
import { Button, Text } from "native-base";
import * as React from "react";
import { StyleSheet, View } from "react-native";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    // state can be change. so hear data given is initial state of variable
    this.state = {
      dataArray: [],
      operatorArray: [],
      display1: "",
      display2: "0",
      display3: "",
      history: ""
    };
    this.display1Inst = "";
    this.display2Inst = "0";
    this.display3Inst = "";
    this.operatorInst = "+";
    this.operatorRef = ["+", "-", "*", "/"];
    this.history = "";
  }

  initialState = () => {
    this.setState({
      dataArray: [],
      operatorArray: [],
      display1: "",
      display2: "0",
      display3: "",
      history: ""
    });
    this.display1Inst = "";
    this.display2Inst = "0";
    this.display3Inst = "";
    this.operatorInst = "+";
    this.history = "";
  };

  butPress = buttonPressed => {
    if (this.display2Inst.length < 10) {
      // numerical button logic
      if (!this.operatorRef.includes(buttonPressed)) {
        this.display2Inst = Number(
          this.display2Inst.concat(buttonPressed)
        ).toString();
        this.setState({
          display2: this.display2Inst
        });
      } // operator button logic
      else {
        this.operatorInst = buttonPressed;
        this.display1Inst = this.display3Inst;

        this.history = this.history.concat(
          this.display2Inst + this.operatorInst
        );

        this.setState({
          display1: this.display3Inst,
          display2: this.display2Inst,
          history: this.history
        });
        this.display2Inst = "";
      }
    }
    this.calculate();
  };

  calculate = () => {
    if (this.display1Inst === "") {
      this.display1Inst = "0";
    }

    let operationString =
      this.display1Inst + "" + this.operatorInst + "" + this.display2Inst;

    if (this.operatorRef.includes(operationString.slice(-1))) {
      operationString = operationString.slice(0, -1);
    }
    this.display3Inst = Math.evaluate(operationString).toString();
    this.setState({
      display1: this.display1Inst,
      display2: this.operatorInst + this.display2Inst,
      display3: "= " + this.display3Inst
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.displayArea}>
          <View style={styles.displayHistory}>
            <Text
              style={{
                fontSize: 20,
                color: "black"
              }}
            >
              {this.state.history}
            </Text>
          </View>
          <View style={styles.display1}>
            <Text
              style={{
                fontSize: 30,
                color: "black"
              }}
            >
              {this.state.display1}
            </Text>
          </View>
          <View style={styles.display2}>
            <Text
              style={{
                fontSize: 30,
                color: "black"
              }}
            >
              {this.state.display2}
            </Text>
          </View>
          <View style={styles.displayResult}>
            <Text
              style={{
                fontSize: 35,
                color: "black"
              }}
            >
              {this.state.display3}
            </Text>
          </View>
        </View>

        <View style={styles.body}>
          <Button
            light
            bordered
            onPress={() => this.butPress("7")}
            style={styles.button}
          >
            <Text style={styles.text}>7</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("8")}
            style={styles.button}
          >
            <Text style={styles.text}>8</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("9")}
            style={styles.button}
          >
            <Text style={styles.text}>9</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("/")}
            style={styles.button}
          >
            <Text style={styles.text}>/</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("4")}
            style={styles.button}
          >
            <Text style={styles.text}>4</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("5")}
            style={styles.button}
          >
            <Text style={styles.text}>5</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("6")}
            style={styles.button}
          >
            <Text style={styles.text}>6</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("*")}
            style={styles.button}
          >
            <Text style={styles.text}>*</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("1")}
            style={styles.button}
          >
            <Text style={styles.text}>1</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("2")}
            style={styles.button}
          >
            <Text style={styles.text}>2</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("3")}
            style={styles.button}
          >
            <Text style={styles.text}>3</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("-")}
            style={styles.button}
          >
            <Text style={styles.text}>-</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.initialState()}
            style={styles.button}
          >
            <Text style={styles.text}>C</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("0")}
            style={styles.button}
          >
            <Text style={styles.text}>0</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.calculate()}
            style={styles.button}
          >
            <Text style={styles.text}>=</Text>
          </Button>
          <Button
            light
            bordered
            onPress={() => this.butPress("+")}
            style={styles.button}
          >
            <Text style={styles.text}>+</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#b3cde0"
  },

  displayArea: {
    flex: 3,
    alignItems: "flex-end",
    justifyContent: "center"
  },
  displayHistory: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start"
  },
  display1: {
    flex: 2,
    justifyContent: "center"
  },

  display2: {
    flex: 2,
    justifyContent: "center"
  },
  displayResult: {
    flex: 2,
    justifyContent: "center",
    borderTopColor: "black",
    borderTopWidth: 1
  },
  body: {
    flex: 4,
    flexDirection: "row",
    flexWrap: "wrap",
    alignContent: "flex-end"
  },

  button: {
    width: "25%",
    height: "25%",
    borderColor: "#005b96",
    paddingLeft: "3%"
  },
  text: {
    color: "black",
    fontSize: 25
  }
});
